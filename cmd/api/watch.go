package main

import (
	"fmt"
	"net/http"
	"time"
	"vintage-watches/internal/data"
	"vintage-watches/internal/validator"
)

func (app *application) createWatchHandler(w http.ResponseWriter, r *http.Request) {
	var input struct {
		Brand        string     `json:"brand"`
		Size         int32      `json:"size"`
		CaseMaterial string     `json:"caseMaterial"`
		Price        data.Price `json:"price"`
	}
	err := app.readJSON(w, r, &input)
	if err != nil {
		app.badRequestResponse(w, r, err)
		return
	}

	watch := &data.Watch{
		Brand:        input.Brand,
		Size:         input.Size,
		CaseMaterial: input.CaseMaterial,
		Price:        input.Price,
	}

	v := validator.New()
	if data.ValidateWatch(v, watch); !v.Valid() {
		app.failedValidationResponse(w, r, v.Errors)
		return
	}
	fmt.Fprintf(w, "%+v\n", input)
}
func (app *application) showWatchHandler(w http.ResponseWriter, r *http.Request) {
	id, err := app.readIDParam(r)
	if err != nil {
		app.notFoundResponse(w, r)
		return
	}
	watch := data.Watch{
		ID:           id,
		CreatedAt:    time.Now(),
		Brand:        "Casio",
		Size:         102,
		CaseMaterial: "Stainless Steel",
		Price:        1000,
	}
	err = app.writeJSON(w, http.StatusOK, envelope{"watch": watch}, nil)
	if err != nil {
		app.serverErrorResponse(w, r, err)
	}

}
