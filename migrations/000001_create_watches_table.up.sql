CREATE TABLE IF NOT EXISTS watches (
    id bigserial PRIMARY KEY,
    created_at timestamp(0) with time zone NOT NULL DEFAULT NOW(),
    brand text NOT NULL,
    size integer NOT NULL,
    case_material text NOT NULL,
    price integer NOT NULL
);
