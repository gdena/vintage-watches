package data

import (
	"time"
	"vintage-watches/internal/validator"
)

type Watch struct {
	ID           int64     `json:"id"`
	CreatedAt    time.Time `json:"-"`
	Brand        string    `json:"brand,omitempty"`
	Size         int32     `json:"size,omitempty"`
	CaseMaterial string    `json:"caseMaterial,omitempty"`
	Price        Price     `json:"price,omitempty,string"`
}

func ValidateWatch(v *validator.Validator, watch *Watch) {
	v.Check(watch.Brand != "", "brand", "must be provided")
	v.Check(len(watch.Brand) <= 500, "brand", "must not be more than 500 bytes long")
	v.Check(watch.Size != 0, "size", "must be provided")
	v.Check(watch.Price != 0, "price", "must be provided")
	v.Check(watch.Price > 0, "price", "must be a positive integer")

}
